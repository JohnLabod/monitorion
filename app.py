import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from actions.main import MainWindow

if __name__ == '__main__':
	window = MainWindow()
	window.connect("destroy", Gtk.main_quit)
	provider = Gtk.CssProvider()
	provider.load_from_file('views/views.css')
	Gtk.StyleContext.add_provider_for_screen(Gdk.Screen.get_default(), provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
	window.show_all()
	Gtk.main()
