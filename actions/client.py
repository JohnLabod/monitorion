from db.models.Client import Client
from actions.vnc import start_vnc
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class ClientContainer():
	parent = None
	container = None
	client_grid = None
	def __init__(self, parent, builder):
		self.parent = parent
		self.container = builder.get_object("container")
		self.client_grid = builder.get_object("client_grid")
		
	def list_clients_on_page(self):
		for client in Client.select():
			button = Gtk.Button(label=client.name)
			button.connect("clicked", start_vnc, client.address)
			self.client_grid.add(button)
			
	def add_client(self):
		builder = Gtk.Builder.new_from_file('views/modal/add_client.glade')
		dialog = builder.get_object("add-client-dialog")
		name_input = builder.get_object("name-entry")
		address_input = builder.get_object("address-entry")
		dialog.show_all()

		response = dialog.run()
		if response == Gtk.ResponseType.OK:
			name = name_input.get_text()
			address = address_input.get_text()
			Client.create(name=name, address=address)
			
			button = Gtk.Button(label=name)
			button.connect("clicked", start_vnc, address)
			self.client_grid.add(button)
			self.client_grid.show_all()
		dialog.destroy()