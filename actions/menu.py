class MenuContainer():
	parent = None
	container = None
	def __init__(self, parent, builder):
		self.parent = parent
		self.container = builder.get_object("menu_items_container")
		
		add_client_button = builder.get_object("add_client_button")
		add_client_button.connect("clicked", self.parent.add_client)
		