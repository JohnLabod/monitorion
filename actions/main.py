import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from actions.menu import MenuContainer
from actions.client import ClientContainer
from actions.info import InfoContainer

class MainWindow(Gtk.Window):
	clients = None
	menu = None
	info = None
	def __init__(self):
		Gtk.Window.__init__(self, title="Monitorion")
		self.set_default_size(1200, 900)
		
		builder = Gtk.Builder.new_from_file('views/main.glade')
		container = builder.get_object("container")
		self.add(container)
		
		self.clients = ClientContainer(self, builder)
		self.clients.list_clients_on_page()
		
		self.menu = MenuContainer(self, builder)
		
		self.info = InfoContainer(self, builder)
	
	def add_client(self, widget):
		self.clients.add_client()