from db.models.Client import Client
from peewee import *
import os

# Remove existing db
os.remove('db/dev.db')

database = SqliteDatabase('db/dev.db')
database.connect()

#Recreate the tables
database.create_tables([Client])

#Seeds
Client.create(name="laboddesktop", address="192.168.1.167")