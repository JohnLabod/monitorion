from peewee import *

database = SqliteDatabase('db/dev.db')

class BaseModel(Model):
	class Meta:
		database = database