from peewee import *
from db.models.Base import BaseModel

class Client(BaseModel):
	name = CharField()
	address = CharField()
	
	class Meta:
		table_name = 'clients'